// Import the react and reactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';
import Message from "./Message";

// Create a react component
const App = () => {

  return (
    <div className="ui container comments">

      <Segment>
        <div className="ui icon header">
          <i className="pdf file outline icon"></i>
          No document are listed for this customer.
        </div>
        <div className="ui primary button">
          Add Document
        </div>
      </Segment>
      <Segment>
        <h4 className="ui header">
          For your Information
        </h4>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
        </p>
      </Segment>

      <ApprovalCard>
        <div>
          <h4>Warning!</h4>
          Are you sure want to do this?
        </div>
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author="Pooja"
          timeAge="Today at 2:45 PM"
          content="Hi there, Pooja"
          avatar={faker.image.avatar()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author="Jignesh"
          timeAge="Today at 3:45 PM"
          content="Hi there, Jignesh"
          avatar={faker.image.avatar()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author="Brinju"
          timeAge="Today at 4:45 PM"
          content="Hi there, Brinju"
          avatar={faker.image.avatar()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author="Amit"
          timeAge="Today at 5:45 PM"
          content="Hi there, Amit"
          avatar={faker.image.avatar()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author="Ashish"
          timeAge="Today at 6:45 PM"
          content="Hi there, Ashish"
          avatar={faker.image.avatar()}
        />
      </ApprovalCard>
      <Message
        header="Change in service 1"
        messageText="We have just updated our services11."
      />
      <Message
        header="Change in service 2"
        messageText="We have just updated our services22."
      />
      <Message
        header="Change in service 3"
        messageText="We have just updated our services33."
      />
    </div>
  );
}

const Segment = props => {
  return (
    <div className="ui placeholder segment">
      {props.children}
    </div>
  )
}

// Take the react component and show it on the screen
ReactDOM.render( <App />, document.querySelector('#root'));
