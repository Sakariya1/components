// Import the react and reactDOM libraries
import React from 'react';

// Create a react component
const Message = props => {

  return (
    <div className="ui message">
      <div className="header">
        {props.header}
      </div>
      <p className="extra content">
        {props.messageText}
      </p>
    </div>
  );
}

export default Message;
